@echo off

echo Run default timer 60 seconds
node stream.js

echo Run custom timer 30 seconds
node stream.js 30
