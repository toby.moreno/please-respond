// Total # of RSVPs received
// Date of Event furthest into the future
// URL for the Event furthest into the future
// The top 3 number of RSVPs received per Event host-country

// sample output
// 100,2019-04-17 12:00,https://www.meetup.com/UXSpeakeasy/events/258247836/,us,40,uk,18,jp,12
// result
// 81,2019-06-22 2:00,https://www.meetup.com/Taunton-and-Surrounds-Social-Family/events/260732349/,gb,44,es,19,in,12
// 73,2019-11-24 05:00,https://www.meetup.com/Nerdy-30s-Ladies-of-St-Louis/events/260926478/,us,59,nl,13,gb,1
const fs = require('fs');
const WebSocket = require('ws');

var ws;

let total = 0;
let newestDate = new Date();
let objEvent;
let dictionary = {};
let timeout = 1000 * 60; // set to 1 minute as default

// make timeout configurable by reading the first command line parameter
// syntax node stream.js 30
// for 30 seconds

if(process.argv.length == 3) {
    val = process.argv[2];

    if(!isNaN(val)) {
        timeout = 1000 * parseInt(val);
        // console.log(timeout);
    }
}

// console.log(process.argv.length);

const wsserver = "stream.meetup.com/2/rsvps";

try {
    ws = new WebSocket(`ws://${wsserver}`);
    ws.on('message', function (event) {
        try {
            var obj = JSON.parse(event);
            total++;

            var d = new Date(obj.event.time);
            if( d > newestDate ) {
                newestDate = d;
                objEvent = obj;
                // console.log(newestDate);
            }

            if(!(objEvent.group.group_country in dictionary)) {
                dictionary[objEvent.group.group_country] = 0;
            }
            dictionary[objEvent.group.group_country]++;

        }
        catch (err) {
            console.log(event);
            console.log(err);
        }
    });

    ws.on('open', function open() {
        // console.log('connected');
        setTimeout(function() {
            // Create items array
            var items = Object.keys(dictionary).map(function(key) {
                return [key, dictionary[key]];
            });
            
            // Sort the array based on the second element
            items.sort(function(first, second) {
                return second[1] - first[1];
            });

            let top3 = items.map((r) => `${r[0]},${r[1]}` );
            if(top3.length > 3) { 
                top3 = top3.slice(0,3);
            }
            
            console.log(`${total},${newestDate.getFullYear()}-${("0" +(newestDate.getMonth()+1)).slice(-2)}-${("0" + newestDate.getDate()).slice(-2)} ${("0" + newestDate.getHours()).slice(-2)}:${ ("0" + newestDate.getMinutes()).slice(-2)},${objEvent.event.event_url},${top3.toString()}`);

            ws.close();

        }, timeout);
    });


} catch (err) {
    console.log(err);
}